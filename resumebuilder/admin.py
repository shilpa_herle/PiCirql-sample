from django.contrib import admin
from .models import UserProfile
from .models import UserEmployment

admin.site.register(UserProfile)
admin.site.register(UserEmployment)
