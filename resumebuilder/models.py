from django.db import models
from django.contrib.auth.models import User

# UserProfile database model with user_email as foreign key
# allows one profile per user_email
class UserProfile(models.Model):
    user_email = models.ForeignKey(User, on_delete=models.CASCADE)
    current_CTC = models.CharField(max_length=200)
    expected_CTC = models.CharField(max_length=200)
    total_exp_years = models.IntegerField()
    total_exp_months = models.IntegerField()
    job_title = models.CharField(max_length=200)
    prof_summary = models.TextField()

# UserEmployment database model with user_email as foreign key
# allows multiple employment details per user_email 
class UserEmployment(models.Model):
    user_email = models.ForeignKey(User, on_delete=models.CASCADE)
    company_name = models.CharField(max_length=200)
    loc_city = models.CharField(max_length=200)
    loc_country = models.CharField(max_length=200)
    job_title = models.CharField(max_length=200)
    job_role = models.CharField(max_length=200)
    period_from = models.DateField()
    period_to = models.DateField()
    is_current = models.BooleanField()
    desc = models.TextField()
