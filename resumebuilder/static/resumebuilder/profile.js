var reloadProfile = function(data) {
    $('#jobtitle_main').text(data.jobtitle);
    $('#prosum_main').text(data.summary);
    $('#curCTC_main').text(data.curCTC);
    $('#expCTC_main').text(data.expCTC);
    $('#expYears_main').text(data.expYears);
    $('#expMonths_main').text(data.expMonths);
}

var deleteRow = function(uemail, ucomp) {
    console.log(uemail);
    console.log(ucomp);
    $.post("/delete_employment/", { useremail: uemail, usercomp: ucomp })
    .done(function() {
        document.location.reload(true);
    });
}

var saveProfile = function() {
    var saveData = {
       email: $('#email').text(),
       jobtitle : $('#jobtitle').val(),
       summary : $('#prosum').val(),
       curCTC : $('#curCTC').val(),
       expCTC : $('#expCTC').val(),
       expYears : $('#expYears').val(),
       expMonths : $('#expMonths').val()
    };
    if (saveData['expYears'] == '' || saveData['expMonths'] == '') {
        $('#message').text('Please do not leave Years/Month fields blank');
    } else if (parseInt(saveData['expMonths']) < 0 || parseInt(saveData['expMonths']) > 12) {
        $('#message').text('Months field takes value in the range 0 and 12');
    } else {
        $('#message').text('');
        $('#saveButton').attr('disabled', 'disabled');
        $.post("/profile_save/", { data: JSON.stringify(saveData) })
         .done(function() {
            // Close modal
            $('#saveButton').removeAttr('disabled');
            $('#myProfile').modal('hide');
            reloadProfile(saveData);
        });
    }
}

var saveEmployment = function() {
    var saveEmpData = {
        email: $('#email').text(),
        company: $('#comp').val(),
        locCity: $('#locCity').val(),
        locCountry: $('#locCountry').val(),
        title: $('#title').val(),
        role: $('#role').val(),
        fDate: $('#fromDate').val(),
        tDate: $('#toDate').val(),
        isCurrent: $('#currentCheck').is(':checked'),
        desc: $('#desc').val()
    };
    if ((moment(saveEmpData['fDate']).isValid()) && (moment(saveEmpData['tDate']).isValid())) {
        $('#message').text('');
        $('#saveEmp').attr('disabled','disabled');
        $.post("/employment_save/", { data: JSON.stringify(saveEmpData) })
        .done(function() {
            // Close modal
            $('#saveEmp').removeAttr('disabled');
            $('#myEmpModal').modal('hide');
            document.location.reload(true);
        });
    } else {
        $('#message').text('Invalid input FromDate/ToDate! Please check.');
    }
}

