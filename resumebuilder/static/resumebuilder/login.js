var checkPasswd = function() {
    var pass = $('#pswd').val();
    var cnfm = $('#cfpswd').val();
    if (cnfm === pass) {
        $(':input[type="submit"]').prop('disabled', false);
        $('#passmismatch').text('');
    } else {
        $('#passmismatch').text('Passwords do not match!');
    }
}

