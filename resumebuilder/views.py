from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, reverse
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from .models import UserProfile, UserEmployment
from .utils import render_to_pdf
import hashlib
import json

# Render login page with error as well as success messages as warnings on page
# Validate login if called using POST
# error-1 : Registration failed
# error-2 : Login failed
# success-1: Registration successful
def login (request):
    if request.method == 'GET':
        # Render login page
        error = request.GET.get('error', None)
        success = request.GET.get('success', None)
        return render_to_response('login.html', {
            'error': error, 'success': success })
    elif request.method == 'POST':
        # Validate login and redirect to profile
        # Get POST parameters
        email = request.POST.get('email', '')
        passwd = request.POST.get('pwd', '')
        # filter User database with email as primary key
        auth = authenticate(username=email, password=passwd)
        if auth is not None:
            # Start session
            auth_login(request, auth)
            # Can use a 'next' parameter here too
            return redirect('profile')
        else:
            return redirect(reverse('login') + '?error=2')
    else:
        return render_to_response('login.html')

# Logout view
def logout (request):
    auth_logout(request)
    return redirect('login')

# render login with appropriate warning/info messages after validating registeration fields
def register (request):
    # Get POST parameters
    uname = request.POST.get('usrname', '')
    uemail = request.POST.get('usremail', '')
    upswd = request.POST.get('usrpswd', '')
    # Check for existing user and error if exists
    user = User.objects.filter(username=uemail)
    if user.count() == 0:
        # Create Django standard User object
        user = User.objects.create_user(uemail,uemail,upswd,first_name=uname)
        user.save()
        return redirect(reverse('login') + '?success=1')
    else:
        return redirect(reverse('login') + '?error=1')

# render user profile page after validating email and password fields
@login_required
def profile (request):  
    download = request.GET.get('download')
    # Get user from standard session
    user_profile = UserProfile.objects.filter(user_email=request.user)
    if user_profile.count() > 0:
        user_p = user_profile[0]
    else:
        user_p = UserProfile(user_email = request.user, current_CTC = "-", 
        expected_CTC = "-", total_exp_years = 0, 
        total_exp_months = 0, job_title = "-", 
        prof_summary = "-")
        user_p.save()
    user_emp_rows = UserEmployment.objects.filter(user_email=request.user)
    if download:
        pdf = render_to_pdf('profile_download.html', {'user_profile': user_p, 'user': request.user, 'user_emp_array': user_emp_rows, 'downloadable': True})
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "%s.pdf" %(request.user.first_name)
            content = "attachment; filename='%s'" %(filename)
            response['Content-Disposition'] = content
            return response
        else:
            return HttpResponse("Not found")
    else:
        return render_to_response('profile.html', { 'user_profile': user_p, 'user': request.user, 'user_emp_array' : user_emp_rows })

# save employment data to UserEmployment database model 
@login_required
def employment_save(request):
    # Get POST parameters
    data = request.POST.get('data')
    employ = json.loads(data)
    email = employ['email']
    company = employ['company']
    locCity = employ['locCity']
    locCountry = employ['locCountry']
    title = employ['title']
    role = employ['role']
    fromDate = employ['fDate']
    toDate = employ['tDate']
    isCurrent = employ['isCurrent']
    desc = employ['desc']
    user = request.user
    empRow = UserEmployment(user_email = user, company_name = company, loc_city = locCity,
        loc_country = locCountry, job_title = title, job_role = role, period_from = fromDate, 
        period_to = toDate, is_current = isCurrent, desc = desc)
    empRow.save()
    return JsonResponse({ 'success': 1 })

# save profile settings data to UserProfile database model
@login_required
def profile_save (request):
    # Get POST parameters
    data = request.POST.get('data')
    profile = json.loads(data)
    email = profile['email']
    jobtitle = profile['jobtitle']
    summary = profile['summary']
    curCTC = profile['curCTC']
    expCTC = profile['expCTC']
    expYears = profile['expYears']
    expMonths = profile['expMonths']
    user = request.user
    profileRow = UserProfile.objects.filter(user_email=user)
    if profileRow.count() == 0:
        # create a new row for the first time 
        urow = UserProfile(user_email = user, current_CTC = curCTC, 
            expected_CTC = expCTC, total_exp_years = expYears, 
            total_exp_months = expMonths, job_title = jobtitle, 
            prof_summary = summary)
        urow.save()
    else:
        # update the row with new settings
        obj = UserProfile.objects.get(user_email=user)
        obj.current_CTC = curCTC 
        obj.expected_CTC = expCTC
        obj.total_exp_years = expYears
        obj.total_exp_months = expMonths
        obj.job_title = jobtitle
        obj.prof_summary = summary
        obj.save()
    return JsonResponse({'success': 1})

# delete each employment row from database upon delete request
@login_required
def delete_employment (request):
    # Get POST parameters
    useremail = request.POST.get('useremail')
    usercomp = request.POST.get('usercomp')
    user = UserEmployment.objects.filter(user_email=request.user, company_name=usercomp)
    for row in user:
        row.delete()
    return JsonResponse({'success':1})
